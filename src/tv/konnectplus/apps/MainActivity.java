package tv.konnectplus.apps;

import tv.konnectplus.apps.FirstFragment.FragmentCommunicator;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

public class MainActivity extends FragmentActivity implements FragmentCommunicator {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		/*
		 * HomeFragment homeFragment=new HomeFragment(); FragmentManager manager=getSupportFragmentManager();
		 * FragmentTransaction transaction=manager.beginTransaction();
		 * transaction.add(R.id.main_container,homeFragment,"HomeFragment"); transaction.commit();
		 */

	}

	@Override
	public void respond(String string) {
		FragmentManager manager = getSupportFragmentManager();
		SecondFragment secondFragment = (SecondFragment) manager.findFragmentById(R.id.second_fragment);

		secondFragment.changeText(string);

	}

}
