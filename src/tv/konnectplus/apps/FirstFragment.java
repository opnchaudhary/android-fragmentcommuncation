package tv.konnectplus.apps;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class FirstFragment extends Fragment implements OnClickListener {
	Button btn;

	int counter = 0;

	FragmentCommunicator communicator;

	public interface FragmentCommunicator {

		public void respond(String string);

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Don't access viewHeirarchy here because Activity's on create may/may not be finished
		// instead use if for background threads for long running operations
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// We should access and modify UI elements here
		communicator = (FragmentCommunicator) getActivity();
		btn = (Button) getActivity().findViewById(R.id.button1);
		btn.setOnClickListener(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// We should return viewHeirarchy for the fragment
		return inflater.inflate(R.layout.fragment_first, container, false);

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// We should use this place to store state in bundle so that we can restore the state again
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button1:
			counter++;
			StringBuilder response = new StringBuilder("This button was clicked ");
			response.append(counter);
			response.append(" times.");
			communicator.respond(response.toString());
			break;
		default:
			break;
		}

	}

}
